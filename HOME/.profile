if [ -z "${_PROFILE_SOURCED+1}" ]; then

[ -n "${ZSH_ARGZERO:-}" ] && setopt +o nomatch
umask 027
for d in "$HOME/.cargo/bin" "$HOME/bin" "$HOME/.local/bin"; do
	[ -d "$d" ] && PATH="$d:$PATH"
done
export PATH
for d in "$HOME/Repos"* "$HOME/Documents/current/repos"* "$HOME/Pictures/inkscape-repos"; do
	[ -d "$d" ] && GITREPOPATH="$d:$GITREPOPATH"
done
GITREPOPATH="${GITREPOPATH%:}"
export GITREPOPATH
export LESS="--tabs=4 --no-init"
export PASSWORD_STORE_ENABLE_EXTENSIONS=true
export _PROFILE_SOURCED=''

fi
