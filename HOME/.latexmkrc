$pdf_mode = 1;
$pdf_previewer = "start zathura %S 2>/dev/null";
$pdflatex = 'xelatex --shell-escape %O %S';
$out_dir = 'build';
$preview_continuous_mode = 1;
# vim: set syntax=perl

