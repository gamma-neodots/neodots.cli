# reload config with prefix, shift+r
bind R source-file ~/.tmux.conf 

# change prefix to Ctrl+a
unbind-key C-b
set -g prefix C-a
bind-key C-a send-prefix

# escape time of 50 ms
set -sg escape-time 50
set -g default-terminal 'screen-256color'
set -ga terminal-overrides ',xterm-256color:Tc,*:Ss=\E[%p1%d q:Se=\E[2 q'

# Version-specific commands [grumble, grumble]
# See: https://github.com/tmux/tmux/blob/master/CHANGES
run-shell "tmux setenv -g TMUX_VERSION $(tmux -V | cut -c 6-)"

if-shell -b '[ "$(echo "$TMUX_VERSION < 2.1" | bc)" = 1 ]' \
	"set -g mouse-select-pane on; set -g mode-mouse on; \
	 set -g mouse-resize-pane on; set -g mouse-select-window on; \
	 set -g default-terminal 'screen-256color'"

# In version 2.1 "mouse" replaced the previous 4 mouse options
if-shell -b '[ "$(echo "$TMUX_VERSION >= 2.1" | bc)" = 1 ]' \
	"set -g mouse on; set -g default-terminal 'tmux-256color'"

# UTF8 is autodetected in 2.2 onwards, but errors if explicitly set
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.2" | bc)" = 1 ]' \
	"set -g utf8 on; set -g status-utf8 on; set -g mouse-utf8 on"

# copy mode < 2.4
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.4" | bc)" = 1 ]' \
	"bind-key -t vi-copy p paste-buffer; \
	 bind-key -t vi-copy y copy-selection"

# In version 2.4, key mode tables were changed
if-shell -b '[ "$(echo "$TMUX_VERSION >= 2.4" | bc)" = ]' \
	"bind-key -T copy-mode-vi p paste-buffer; \
	 bind-key -T copy-mode-vi y send-keys -X copy-selection"

# Terminal title 
set-option -g set-titles on
set-option -g set-titles-string "#{client_termname} - #{session_group}[#{pane_id}] #T"
set-window-option -g automatic-rename on
set-window-option -g mode-keys vi


# hjkl pane traversal
# > w/prefix
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
# > prefixless --
bind -n M-h select-pane -L
bind -n M-j select-pane -D
bind -n M-k select-pane -U
bind -n M-l select-pane -R

# HJKL pane resize
# > w/prefix
bind H resize-pane -L
bind J resize-pane -D
bind K resize-pane -U
bind L resize-pane -R
# > prefixless --
bind -n M-H resize-pane -L
bind -n M-J resize-pane -D
bind -n M-K resize-pane -U
bind -n M-L resize-pane -R

# Move windows
bind P swap-window -t :-
bind N swap-window -t :+

bind-key t next-window
bind-key T previous-window

setw -g aggressive-resize on

# Status changes
set -g status-justify "left"
set -g status "on"
set -g status-left-style "none"
set -g message-command-style "fg=colour231,bg=colour31"
set -g status-right-style "none"
set -g pane-active-border-style "fg=colour254"
#set -g status-utf8 "on"
set -g status-style "bg=colour234,none"
set -g message-style "fg=colour231,bg=colour31"
set -g pane-border-style "fg=colour0"
#set -g status-right-length "100"
#set -g status-left-length "100"

set -g status-bg colour0
set -g status-fg default
# Double %% because of strftime
# set status-right '[]#(echo "${SSH_CONNECTION%% *} " | cut -d\  -f 3-)#{host_short}'
set -g status-left "#[fg=colour16,bg=colour7,bold] #S #[fg=colour7,bg=colour0,nobold,nounderscore,noitalics]" 
set -g status-right '#[fg=colour10]#{prefix_highlight}#{pane_id}#[fg=colour15]  #[fg=colour6,bold]#(ip addr show | sed -nr -e "/inet .*(zt|tun)/{s/.* ([0-9.]+)\/.*/\1/p}")#[fg=colour15,nobold]:#{host_short}'
# set-window-option -g status-left-bg default
# set-window-option -g status-left-fg cyan
setw -g window-status-format "#[fg=colour9,bg=colour0] #I#F#[fg=colour250,bg=colour0] #W "
setw -g window-status-current-format "#[fg=colour0,bg=colour9,nobold,nounderscore,noitalics]#[fg=colour07,bg=colour9] #I#F#[fg=colour231,bg=colour9,bold] #W #{?pane_synchronized,#[fg=colour190](SYNC),} #[fg=colour9,bg=colour0,nobold,nounderscore,noitalics]"
set -g pane-active-border-style "fg=colour9"
